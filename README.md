# gofundme-slackbot
Posts a GoFundMe's progress to a Slack channel

* Needs Python 3, python-requests and python-slackclient(slack_sdk) for Python 3.
* Install needed modules with your system's package manager or with pip install -r requirements.txt.
* Set the goal, url, slack token, and slack channel in config.ini. Copy config.ini-example and edit.
* Set up a cron job to run the bot when desired.
